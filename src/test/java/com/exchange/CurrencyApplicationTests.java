package com.exchange;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.exchange.controller.CurrencyController;
import com.exchange.exception.CustomException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.exchange.repository.ICurrencyRepository;
import com.exchange.model.Conversion;
import com.exchange.service.impl.CurrencyService;
import com.exchange.util.CurrencyUtils;

@SpringBootTest
public class CurrencyApplicationTests {

	public static final String ACCESS_KEY = "2fe785265ba7465e09fad3cea3baedcc";
	public static final String BASE_URL = "http://data.fixer.io/api/";
	public static final String ENDPOINT = "latest";

	@InjectMocks
	private CurrencyService currencyService;
	
	@Mock
	private RestTemplate restTemplate;
	
	@Mock
	private ICurrencyRepository currencyRepository;

	CurrencyController currencyController = new CurrencyController();

	@Rule
	public final ExpectedException thrown = ExpectedException.none();
	
	@Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }
	
	@Test
	public void testExchangeRate() {
		String  sourceCurrency = "USD", targetCurrency = "TRY";
		String rateString = "5.0";
		String apiUrl  = BASE_URL + ENDPOINT + "?access_key=" + ACCESS_KEY + "&symbols="+targetCurrency + "," + sourceCurrency;
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
        ResponseEntity<String> res =new ResponseEntity<>("{\"success\":true,\"timestamp\":1638097443,\"base\":\"EUR\",\"date\":\"2021-11-28\",\"rates\":{\"TRY\":10,\"USD\":2}}", HttpStatus.OK);
		when(restTemplate.exchange(apiUrl, HttpMethod.GET, entity, String.class)).thenReturn(res);
		assertEquals(rateString, currencyService.getExchangeRate(sourceCurrency, targetCurrency));
	}

	@Test(expected = CustomException.class)
	public void testNullIdAndRecordDate(){
		currencyController.exchangeList(null,null,null,null);
	}

	@Test(expected = CustomException.class)
	public void testIdAndDateFormatAtSameTime(){
		currencyController.exchangeList(1L,LocalDate.parse("2021-10-11"),null,null);
	}

	@Test
	public void testConversion(){
		Long transactionId = 124L;
		Conversion conversion = new Conversion(transactionId ,BigDecimal.valueOf(100),"TRY","USD",LocalDate.now());
		when(currencyRepository.save(conversion)).thenReturn(conversion);
		assertEquals(transactionId,currencyRepository.save(conversion).getId());
	}
	


}
