package com.exchange.exception;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

@RestControllerAdvice
@Slf4j
public class Handler {

    @ExceptionHandler({CustomException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse handleCustomException(CustomException e) {
        log.error("Runtime Exception thrown: {}", e);
        return new ErrorResponse(e.getCode().getCode(), e.getDetails());
    }

    @ExceptionHandler({MethodArgumentTypeMismatchException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse handleMethodArgumentTypeMismatchException(Exception e) {
        log.error("Invalid format exception: {}", e);
        return new ErrorResponse(ErrorCodes.DATE_WRONG_FORMAT.getCode(), ErrorCodes.Constants.DATE_WRONG_FORMAT_MSG);
    }

}
