package com.exchange.exception;

import lombok.Getter;

@Getter
public class CustomException extends RuntimeException {

    private final ErrorCodes code;
    private String details;

    public CustomException(ErrorCodes code) {
        super(code.name());
        this.code = code;
        this.details = code.getMsg();
    }

}
