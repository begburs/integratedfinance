package com.exchange.exception;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum ErrorCodes {

    AT_LEAST_ID_OR_DATE(1000, Constants.AT_LEAST_ID_OR_DATE_MSG),
    DATE_WRONG_FORMAT(1001, Constants.DATE_WRONG_FORMAT_MSG),
    ONLY_TRANSACTION_ID_OR_DATE(1002, Constants.ONLY_TRANSACTION_ID_OR_DATE_MSG);

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    private final int code;
    private final String msg;

    public static class Constants {
        public final static String AT_LEAST_ID_OR_DATE_MSG = "At least transaction id or date must be provided!";
        public final static String DATE_WRONG_FORMAT_MSG = "Valid format is yyyy-MM-dd!";
        public final static String ONLY_TRANSACTION_ID_OR_DATE_MSG = "Only transaction id or date must be provided at the same time!";
    }
}

