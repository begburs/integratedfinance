package com.exchange.exception;

public class ErrorResponse {


    private String detail;

    private Integer responseCode;

    public ErrorResponse( Integer responseCode, String detail) {
        this.detail = detail;
        this.responseCode = responseCode;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public Integer getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(Integer responseCode) {
        this.responseCode = responseCode;
    }


}
