package com.exchange.controller;

import java.time.LocalDate;
import java.util.List;

import com.exchange.exception.CustomException;
import com.exchange.exception.ErrorCodes;
import com.exchange.model.Rate;
import com.exchange.service.ICurrencyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import com.exchange.model.Conversion;

import javax.validation.constraints.PastOrPresent;


@Controller
public class CurrencyController {
	
	Logger logger = LoggerFactory.getLogger(CurrencyController.class);
	
	@Autowired
	ICurrencyService currencyService;
	
	
	@GetMapping("/exchangeRate")
	@ResponseBody
	public ResponseEntity<Rate> exchangeRate(@RequestParam String baseCurrency , @RequestParam String targetCurrency) {
		return ResponseEntity.ok(new Rate(currencyService.getExchangeRate(baseCurrency,targetCurrency)));
	}
	
	@PostMapping("/exchange")
	@ResponseBody
	public ResponseEntity<Conversion> exchange(@RequestParam String sourceAmount, @RequestParam String sourceCurrency, @RequestParam String targetCurrency) {
		return ResponseEntity.ok(currencyService.convert(sourceAmount, sourceCurrency, targetCurrency));
	}

	@GetMapping("/exchangeList")
	@ResponseBody
	public ResponseEntity<List<Conversion>> exchangeList(@RequestParam(required = false) Long id, @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @PastOrPresent @RequestParam(required = false) LocalDate recordDate,
			@RequestParam(defaultValue = "0") Integer pageNo, @RequestParam(defaultValue = "10") Integer pageSize) {
		if(id == null && recordDate == null) {
			throw new CustomException(ErrorCodes.AT_LEAST_ID_OR_DATE);
		}
		if(id != null && recordDate != null) {
			throw new CustomException(ErrorCodes.ONLY_TRANSACTION_ID_OR_DATE);
		}
		return ResponseEntity.ok(currencyService.findAllConversionByFilter(id, recordDate, pageNo, pageSize ));
	}

}
