package com.exchange.util;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public final class CurrencyUtils {
	
	private CurrencyUtils() {
    }
	
	public static BigDecimal calculateTargetAmount(String rateString , String amountString) {
		BigDecimal rate = new BigDecimal(rateString);
		BigDecimal amount = new BigDecimal(amountString);
		return rate.multiply(amount);
    }

    public static String calculateCurrency(String baseCurrency,String targetCurrency){
		return  String.valueOf(Float.parseFloat(targetCurrency)/Float.parseFloat(baseCurrency));
	};
	
	public static String todayString() {
		String pattern = "dd/MM/yyyy";
		DateFormat format = new SimpleDateFormat(pattern);
		Date today = Calendar.getInstance().getTime();      
		String todayAsString = format.format(today);
		return todayAsString;
    }

}
