package com.exchange.repository;

import java.time.LocalDate;
import java.util.List;

import com.exchange.model.Conversion;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface ICurrencyRepository extends PagingAndSortingRepository<Conversion, String>  {
	
	public Conversion save(Conversion conversion);
	public List<Conversion> findAllByRecordDate(String recorDate, Pageable pageable);
	public List<Conversion> findAllByTransactionId(Long id, Pageable pageable);
	public List<Conversion> findAllByTransactionIdOrRecordDate(Long id, LocalDate recorDate, Pageable pageable);
	
}
