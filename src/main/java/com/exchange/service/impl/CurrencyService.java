package com.exchange.service.impl;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import com.exchange.service.ICurrencyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.exchange.repository.ICurrencyRepository;
import com.exchange.model.Conversion;
import com.exchange.util.CurrencyUtils;

@Service
public class CurrencyService implements ICurrencyService {

	Logger logger = LoggerFactory.getLogger(CurrencyService.class);

	public static final String ACCESS_KEY = "2fe785265ba7465e09fad3cea3baedcc";
	public static final String BASE_URL = "http://data.fixer.io/api/";
	public static final String ENDPOINT = "latest";
	
	@Autowired
	private ICurrencyRepository currencyRepository;
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Override
	public Conversion convert(String sourceAmount, String sourceCurrency, String targetCurrency) {
		
		String rateString = getExchangeRate(sourceCurrency,targetCurrency);
		BigDecimal targetAmount = CurrencyUtils.calculateTargetAmount(rateString, sourceAmount);
		return currencyRepository.save(new Conversion(targetAmount,sourceCurrency,targetCurrency));
	}



	@Override
	public String getExchangeRate(String sourceCurrency, String targetCurrency) {

		String apiUrl  = BASE_URL + ENDPOINT + "?access_key=" + ACCESS_KEY + "&symbols="+targetCurrency + "," + sourceCurrency;
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
        ResponseEntity<String> res = null;
        try {
        	res = restTemplate.exchange(apiUrl, HttpMethod.GET, entity, String.class);
		} catch (Exception e) {
			logger.error("Error while accessing API!", e.getMessage());
		}
        
        HttpStatus httpStatus = res.getStatusCode();
        if(httpStatus.compareTo(HttpStatus.OK) == 0) {
        	 Gson gson = new Gson();
             JsonObject convertedObject = gson.fromJson(res.getBody(), JsonObject.class);
             convertedObject = (JsonObject) convertedObject.get("rates");
             return CurrencyUtils.calculateCurrency(convertedObject.get(sourceCurrency).toString(),convertedObject.get(targetCurrency).toString());
        }
        else {
        	try {
				throw new Exception();
			} catch (Exception e) {
				logger.error("API couldnt return success code", e.getMessage());
			}
        }
        return null;

	}

	@Override
	public List<Conversion> findAllConversionByFilter(Long transactionId, LocalDate recordDate, Integer pageNo, Integer pageSize) {
		Pageable pageable = PageRequest.of(pageNo, pageSize);
		return currencyRepository.findAllByTransactionIdOrRecordDate(transactionId, recordDate, pageable);
	}

}
