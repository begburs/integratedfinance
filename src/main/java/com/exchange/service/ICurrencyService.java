package com.exchange.service;

import java.time.LocalDate;
import java.util.List;
import com.exchange.model.Conversion;

public interface ICurrencyService {
	
	Conversion convert(String sourceAmount, String sourceCurrency, String targetCurrency);
	String getExchangeRate(String baseCurrency, String targetCurrency);
	List<Conversion> findAllConversionByFilter(Long id, LocalDate recordDate, Integer pageNo, Integer pageSize);


}
