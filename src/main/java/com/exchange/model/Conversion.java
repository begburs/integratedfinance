package com.exchange.model;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.data.annotation.CreatedDate;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.*;


@Entity
@Table
public class Conversion {

    @Id
    @GeneratedValue
    private Long transactionId;

    private BigDecimal amount;

    private String sourceCurrency;

	@JsonFormat(pattern = "yyyy-MM-dd")
	@Column(columnDefinition = "TIMESTAMP")
	private LocalDate recordDate;

	private String targetCurrency;

	public Conversion(BigDecimal amount, String sourceCurrency, String targetCurrency) {
		super();
		this.amount = amount;
		this.sourceCurrency = sourceCurrency;
		this.targetCurrency = targetCurrency;
		this.recordDate = LocalDate.now();
	}

	public Conversion(Long id,BigDecimal amount, String sourceCurrency, String targetCurrency, LocalDate localDate) {
		super();
		this.transactionId = id;
		this.amount = amount;
		this.sourceCurrency = sourceCurrency;
		this.targetCurrency = targetCurrency;
		this.recordDate = localDate;
	}

	public Conversion() {
		super();
	}




	public Long getId() {
		return transactionId;
	}

	public void setId(Long id) {
		this.transactionId = id;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getSourceCurrency() {
		return sourceCurrency;
	}

	public void setSourceCurrency(String sourceCurrency) {
		this.sourceCurrency = sourceCurrency;
	}

	public String getTargetCurrency() {
		return targetCurrency;
	}

	public void setTargetCurrency(String targetCurrency) {
		this.targetCurrency = targetCurrency;
	}

	public LocalDate getRecordDate() {
		return recordDate;
	}

	public void setRecordDate(LocalDate recordDate) {
		this.recordDate = recordDate;
	}


}