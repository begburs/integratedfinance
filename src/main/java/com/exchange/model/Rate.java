package com.exchange.model;

import lombok.Data;

@Data
public class Rate {

    private Double value;

    public Rate(String rate) {
        this.value = Double.valueOf(rate);
    }
}
